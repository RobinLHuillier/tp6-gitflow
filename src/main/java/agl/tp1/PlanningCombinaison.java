package agl.tp1;

import java.time.LocalDateTime;

public class PlanningCombinaison {
	private String prof;
	private Groupe groupe;
	public String getProf() {
		return prof;
	}

	public void setProf(String prof) {
		this.prof = prof;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	private LocalDateTime date;
	
	public PlanningCombinaison() {};
	
	public String toString() {
		return date.toString() + ": " + prof + " encadre " + groupe.getNom() + " (sujet: " + groupe.getAffectation().getTitre() + " )";
	}
}
