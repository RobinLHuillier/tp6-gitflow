package agl.tp1;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GestionTER {
	public ArrayList<Groupe> groupes;
	public ArrayList<Sujet> sujets;
	public ArrayList<String> profs;
	
	public GestionTER() {
	  this.groupes = new ArrayList<>();
	  this.sujets = new ArrayList<>();
	}
	
	public void addGroupe(String nom) {
	  this.groupes.add(new Groupe(nom));
	}
	public void addGroupe(Groupe groupe){
		this.groupes.add(groupe);
	}

	public void addSujet(String titre) {
	  this.sujets.add(new Sujet(titre));
	}

	public void addSujet(Sujet sujet) {
		this.sujets.add(sujet);
	}

	public ArrayList<Sujet> getSujets() {
	  return this.sujets;
	}
	public int getNbGroupe() {
	  return this.groupes.size();
	}
	public Groupe getGroupe(int id) {
	  if(id >= 0 && id < this.getNbGroupe()) {
	    return this.groupes.get(id);
	  }
	  return new Groupe("n'existe pas");
	}
	
	public void addProf(String prof) {
		this.profs.add(prof);
	}
	public ArrayList<String> getProfs() {
		return this.profs;
	}
	
	public String toString() {
	  String str = "Liste des groupes:\n----------------\n";
	  for(int i=0; i<this.groupes.size(); i++) {
	    str += this.groupes.get(i).toString() + "\n";
	  }
	  str += "\n\nListe des sujets:\n------------------\n";
	  for(int i=0; i<this.sujets.size(); i++) {
	    str += this.sujets.get(i).toString() + "\n";
	  }
	  return str;
	}
	
	public void serializeGroupe() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    test.writeValue(new File("target/groupes.json"), this.groupes);
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	
	public void serializeSujet() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    test.writeValue(new File("target/sujets.json"), this.sujets);
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	
	public void importGroupe() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    Groupe[] groupesS = test.readValue(new File("target/groupes.json"), Groupe[].class);
	    this.groupes = new ArrayList<>();
	    for(int i=0; i<groupesS.length; i++) {
	      this.groupes.add(groupesS[i]);
	    }
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	
	public void importSujet() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    Sujet[] sujetsS = test.readValue(new File("target/sujets.json"), Sujet[].class);
	    this.sujets = new ArrayList<>();
	    for(int i=0; i<sujetsS.length; i++) {
	      this.sujets.add(sujetsS[i]);
	    }
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	
	public ArrayList<Groupe> getGroupeAvecAffectation() {
		ArrayList<Groupe> groupesAff = new ArrayList<>();
		for(Groupe g: this.groupes) {
			if (g.getAffectation() != null) {
				groupesAff.add(g);
			}
		}		
		return groupesAff;
	}
	
	public ArrayList<PlanningCombinaison> genererPlanning() {
		ArrayList<PlanningCombinaison>  planning = new ArrayList<>();
		ArrayList<Groupe> groupesAff = this.getGroupeAvecAffectation();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
		LocalDateTime horaire = LocalDateTime.parse("2021/12/10 08:00", format);
		final int dureeSoutenance = 45;
		for(Groupe g: groupesAff) {
			PlanningCombinaison plan = new PlanningCombinaison();
			plan.setGroupe(g);
			plan.setProf(g.getProf());
			plan.setDate(horaire);
			planning.add(plan);
			horaire.plusMinutes(dureeSoutenance);
			//il faudrait pouvoir passer au jour suivant, si l'heure dépasse une heure max
		}
		return planning;
	}
}