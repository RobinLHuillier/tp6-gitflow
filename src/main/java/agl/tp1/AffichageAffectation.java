package agl.tp1;

import com.github.javafaker.Faker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class AffichageAffectation {
    public static void affectationsVersHtml(GestionTER ter) {
        try {
            File myObj = new File("affectation.html");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter = new FileWriter("affectation.html");
            String htmlCode = "<style type=\"text/css\">\n" +
                    ".tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}\n" +
                    ".tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}\n" +
                    ".tftable tr {background-color:#d4e3e5;}\n" +
                    ".tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}\n" +
                    ".tftable tr:hover {background-color:#ffffff;}\n" +
                    "</style>";
            htmlCode += "<table class=\"tftable\" border=\"1\">\n";

            String htmlCodeHeader = "<tr><th>Nom des groupes:</th>";
            String htmlCodeRow = "<tr><th>Nom du sujet affecté:</th>";
            for(Groupe groupe : ter.groupes){
                htmlCodeHeader += "<th>" + groupe.nom + "</th>";
                if(groupe.getAffectation() != null) {
                    htmlCodeRow += "<td>" + groupe.getAffectation() + "</td>";
                } else {
                    htmlCodeRow += "<td></td>";
                }
            }
            htmlCodeHeader += "</tr>\n";
            htmlCodeRow += "</tr>\n";

            htmlCode += htmlCodeHeader + htmlCodeRow + "</table>";
            myWriter.write(htmlCode);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static GestionTER generationAleatoireGestionTer(int nombreDeGroupe){
        GestionTER gestionTER = new GestionTER();
        for(int i = 0; i < nombreDeGroupe; ++i) {
            Faker faker = new Faker();
            Groupe g = new Groupe(faker.cat().name());
            Sujet s = new Sujet(faker.book().title());
            gestionTER.addGroupe(g);
            gestionTER.addSujet(s);
            g.setAffectation(s);
        }
        return gestionTER;
    }
}
